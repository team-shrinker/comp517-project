import matplotlib.pyplot as plt

def plot_line_graphs_avazu(lists):
    colors = ['#fa5502', '#ffb894', '#45c45c', '#0859fc', '#79a5fc', '#b00519']

    # Plotting each list    
    plt.plot([20, 5, 1] , lru_avazu_results[0], color=colors[0], label='Avazu, largest, train')
    plt.plot([20, 5, 1] , lru_avazu_results[1], color=colors[1], label='Avazu, 2nd largest, train')
    plt.plot([20, 5, 1] , lru_avazu_results[2], color=colors[2], label='Avazu, random, train')
    plt.plot([20, 5, 1] , lru_avazu_results[3], color=colors[3], label='Avazu, largest, test')
    plt.plot([20, 5, 1] , lru_avazu_results[4], color=colors[4], label='Avazu, 2nd largest, test')
    plt.plot([20, 5, 1] , lru_avazu_results[5], color=colors[5], label='Avazu, random, test')

    plt.gca().invert_xaxis()

    plt.title("LFU Hit Rates - Avazu")
    plt.xlabel("Cache size, percentage of embedding size")
    plt.ylabel("Hit rate")

    # Adding legend
    plt.legend()

    # Show plot
    plt.show()

def plot_line_graphs_criteo(lists):
    colors = [
        '#fc5805', '#fa874d', '#ffb996', '#ffd6c2', '#fc68c8',
        '#0043fc', '#3d71ff', '#81a2fc', '#bfd0ff', '#fa2340'
    ]

    # Plotting each list    
    plt.plot([20, 5, 1] , lru_criteo_results[0], color=colors[0], label='Criteo Kaggle, largest, train')
    plt.plot([20, 5, 1] , lru_criteo_results[1], color=colors[1], label='Criteo Kaggle, 2nd largest, train')
    plt.plot([20, 5, 1] , lru_criteo_results[2], color=colors[2], label='Criteo Kaggle, 3rd largest, train')
    plt.plot([20, 5, 1] , lru_criteo_results[3], color=colors[3], label='Criteo Kaggle, 4th largest, train')
    plt.plot([20, 5, 1] , lru_criteo_results[4], color=colors[4], label='Criteo Kaggle, random, test')
    plt.plot([20, 5, 1] , lru_criteo_results[5], color=colors[5], label='Criteo Kaggle, largest, test')
    plt.plot([20, 5, 1] , lru_criteo_results[6], color=colors[6], label='Criteo Kaggle, 2nd largest, test')
    plt.plot([20, 5, 1] , lru_criteo_results[7], color=colors[7], label='Criteo Kaggle, 3rd largest, test')
    plt.plot([20, 5, 1] , lru_criteo_results[8], color=colors[8], label='Criteo Kaggle, 4th largest, test')
    plt.plot([20, 5, 1] , lru_criteo_results[9], color=colors[9], label='Criteo Kaggle, random, test')

    plt.gca().invert_xaxis()

    plt.title("LFU Hit Rates - Criteo Kaggle")
    plt.xlabel("Cache size, percentage of embedding size")
    plt.ylabel("Hit rate")

    # Adding legend
    plt.legend()

    # Show plot
    plt.show()

lru_avazu_results = [
    [0.8284, 0.7603, 0.6569],
    [0.9401, 0.9217, 0.9089],
    [0.977, 0.6663, 0.2542],
    [0.7643, 0.6161, 0.3111],
    [0.9445, 0.9263, 0.8943],
    [0.8529, 0.4844, 0.1263]
]

lfu_avazu_results = [
    [0.6442, 0.4164, 0.2679],
    [0.9085, 0.8564, 0.8356],
    [0.6058, 0.3906, 0.2144],
    [0.5648, 0.2847, 0.1546],
    [0.9318, 0.8877, 0.8706],
    [0.5513, 0.3338, 0.0852]
]

lru_criteo_results = [
    [0.7827, 0.7134, 0.6429],
    [0.8169, 0.7504, 0.6786],
    [0.842, 0.7778, 0.7043],
    [0.8737, 0.8137, 0.7374],
    [0.9494, 0.7121, 0.3111],
    [0.728, 0.6395, 0.5421],
    [0.7621, 0.6751, 0.5753],
    [0.7874, 0.7014, 0.6],
    [0.82, 0.747, 0.6259],
    [0.8153, 0.443, 0.1267]
]

lfu_criteo_results = [
    [0.7903, 0.7261, 0.6383],
    [0.8244, 0.755, 0.6722],
    [0.8495, 0.7805, 0.7017],
    [0.8812, 0.8192, 0.7437],
    [0.9393, 0.7225, 0.3639],
    [0.7388, 0.6692, 0.5674],
    [0.7729, 0.7026, 0.5975],
    [0.7983, 0.7266, 0.6195],
    [0.8314, 0.7583, 0.6485],
    [0.925, 0.6722, 0.3154]
]

# plot_line_graphs_avazu(lru_avazu_results)
# plot_line_graphs_avazu(lfu_avazu_results)

# plot_line_graphs_criteo(lru_criteo_results)
plot_line_graphs_criteo(lfu_criteo_results)
