from collections import OrderedDict, defaultdict

# one embedding table per categorical variable
NUM_EMBEDDING_TABLES = 22

# mapping index in the test data to the number of unique values for that
# categorical variable
num_unique_vals_test = {
	# ignore this column 0 => there are as many as the number of records
	# as there are rows in the dataset, which means it'll always results 
	# in a cache miss
	0: 4577465,
	1: 8,
	2: 7,
	3: 2826,
	4: 3367,
	5: 23,
	6: 3953,
	7: 202,
	8: 29,
	9: 291760,
	10: 1077200,
	11: 5439,
	12: 5,
	13: 5,
	14: 1258,
	15: 9,
	16: 10,
	17: 241,
	18: 5,
	19: 48,
	20: 163,
	21: 40,
}

num_unique_vals_train = {
	# ignore this column 0 => there are as many as the number of records
	# as there are rows in the dataset, which means it'll always results 
	# in a cache miss
	0: 40428968,
	1: 8,
	2: 8,
	3: 4738,
	4: 7746, # top 5
	5: 27,
	6: 8553, # top 3
	7: 560,
	8: 37,
	9: 2686409, # top 2
	10: 6729487, # top 1
	11: 8252, # top 4
	12: 6,
	13: 5,
	14: 2627,
	15: 9,
	16: 10,
	17: 436,
	18: 5,
	19: 69,
	20: 173,
	21: 61
}

class LRUCache(OrderedDict):
	def __init__(self, capacity):
		"""
		:type capacity: int
		"""
		self.capacity   = capacity
		self.cache_hit  = 0
		self.cache_miss = 0
		self.last_hit = -1
		self.perfect_hit_opportunity = 0
	def get(self, key):
		"""
		:type key: int
		:rtype: int
		"""
		if key not in self:
			return -1
		self.move_to_end(key)
		return self[key]
	def put(self, key, value):
		"""
		:type key: int
		:type value: int
		:rtype: void
		"""
		if key in self:
			# only count hits when the cache is filled
			# (not during process of filling up)
			if len(self) == self.capacity: 
				self.cache_hit += 1
				self.last_hit = 1
				self.perfect_hit_opportunity = 1
			self.move_to_end(key)
		else:
			# only count misses when the cache is filled
			# (not during process of filling up)
			if len(self) == self.capacity:
				self.cache_miss += 1
				self.last_hit = 0
				self.perfect_hit_opportunity = 1
		self[key] = value
		if len(self) > self.capacity:
			self.popitem(last = False)

class LFUCache:
	def __init__(self, capacity: int):
		self.capacity=capacity
		self.minfreq=None
		self.keyfreq={}
		self.freqkeys=defaultdict(OrderedDict)
		self.cache_hit = 0
		self.cache_miss = 0
		self.last_hit = -1
		self.perfect_hit_opportunity = 0
	def get(self, key: int) -> int:
		if key not in self.keyfreq:
			return -1
		freq=self.keyfreq[key]
		val=self.freqkeys[freq][key]
		del self.freqkeys[freq][key]
		if not self.freqkeys[freq]:
			if freq==self.minfreq:
				self.minfreq+=1
			del self.freqkeys[freq]
		self.keyfreq[key]=freq+1
		self.freqkeys[freq+1][key]=val
		return val
	def put(self, key: int, value: int) -> None:
		if self.capacity<=0:
			return
		if key in self.keyfreq:
			# only count hits when the cache is filled
			# (not during process of filling up)
			if self.capacity==len(self.keyfreq):
				self.cache_hit += 1
				self.last_hit = 1
				self.perfect_hit_opportunity = 1
			freq=self.keyfreq[key]
			self.freqkeys[freq][key]=value
			self.get(key)
			return
		if self.capacity==len(self.keyfreq):
			# only count misses when the cache is filled
			# (not during process of filling up)
			self.cache_miss += 1
			self.last_hit = 0
			self.perfect_hit_opportunity = 1
			delkey,delval=self.freqkeys[self.minfreq].popitem(last=False)
			del self.keyfreq[delkey]
		self.keyfreq[key]=1
		self.freqkeys[1][key]=value
		self.minfreq=1

def main(data_source = "train"):
	if data_source not in { "train", "test" }:
		raise Exception("data_source must be in { \"train\", \"test\" }")

	if data_source == "train":
		lru_caches = [
			[
				LRUCache(int(0.2 * num_unique_vals_train[idx])),
				LRUCache(int(0.05 * num_unique_vals_train[idx])),
				LRUCache(int(0.01 * num_unique_vals_train[idx]))
			] for idx in range(NUM_EMBEDDING_TABLES)
		]
		lfu_caches = [
			[
				LFUCache(int(0.2 * num_unique_vals_train[idx]) + 1),
				LFUCache(int(0.05 * num_unique_vals_train[idx]) + 1),
				LFUCache(int(0.01 * num_unique_vals_train[idx]) + 1)
			] for idx in range(NUM_EMBEDDING_TABLES)
		]
		f = open('data/train.csv', 'r')
	else:
		lru_caches = [
			[
				LRUCache(int(0.2 * num_unique_vals_test[idx])),
				LRUCache(int(0.05 * num_unique_vals_test[idx])),
				LRUCache(int(0.01 * num_unique_vals_test[idx]))
			] for idx in range(NUM_EMBEDDING_TABLES)
		]
		lfu_caches = [
			[
				LFUCache(int(0.2 * num_unique_vals_test[idx]) + 1),
				LFUCache(int(0.05 * num_unique_vals_test[idx]) + 1),
				LFUCache(int(0.01 * num_unique_vals_test[idx]) + 1)
			] for idx in range(NUM_EMBEDDING_TABLES)
		]
		f = open('data/test.csv', 'r')
	
	unique_values = defaultdict(set)
	
	lru_perfect_hits_20 = 0
	lru_perfect_hits_5 = 0
	lru_perfect_hits_1 = 0

	lfu_perfect_hits_20 = 0
	lfu_perfect_hits_5 = 0
	lfu_perfect_hits_1 = 0

	lru_perfect_opportunities_20 = 0
	lru_perfect_opportunities_5 = 0
	lru_perfect_opportunities_1 = 0

	lfu_perfect_opportunities_20 = 0
	lfu_perfect_opportunities_5 = 0
	lfu_perfect_opportunities_1 = 0

	line_num = 0
	for line in f:
		# Split the line into a list of values using the separator
		pre_values = line.split(',') # TODO: remove '\n' from the last word
		if data_source == "train":
			values = [pre_values[0]] + pre_values[3:]
		else:
			values = [pre_values[0]] + pre_values[2:]
		for idx in range(NUM_EMBEDDING_TABLES):
			value = values[idx]
			unique_values[idx].add(value)
			for cache_idx in range(3):
				lru_caches[idx][cache_idx].put(value, 1)
				lfu_caches[idx][cache_idx].put(value, 1)
		
		lru_perfect_hit_20 = lru_caches[9][0].last_hit & lru_caches[10][0].last_hit
		lru_perfect_hit_5  = lru_caches[9][1].last_hit & lru_caches[10][1].last_hit
		lru_perfect_hit_1  = lru_caches[9][2].last_hit & lru_caches[10][2].last_hit
	
		lfu_perfect_hit_20 = lfu_caches[9][0].last_hit & lfu_caches[10][0].last_hit
		lfu_perfect_hit_5  = lfu_caches[9][1].last_hit & lfu_caches[10][1].last_hit
		lfu_perfect_hit_1  = lfu_caches[9][2].last_hit & lfu_caches[10][2].last_hit
	
		lru_perfect_hits_20 += lru_perfect_hit_20
		lru_perfect_hits_5  += lru_perfect_hit_5
		lru_perfect_hits_1  += lru_perfect_hit_1
	
		lfu_perfect_hits_20 += lfu_perfect_hit_20
		lfu_perfect_hits_5  += lfu_perfect_hit_5
		lfu_perfect_hits_1  += lfu_perfect_hit_1

		lru_perfect_opportunity_20 = lru_caches[9][0].perfect_hit_opportunity & lru_caches[10][0].perfect_hit_opportunity
		lru_perfect_opportunity_5  = lru_caches[9][1].perfect_hit_opportunity & lru_caches[10][1].perfect_hit_opportunity
		lru_perfect_opportunity_1  = lru_caches[9][2].perfect_hit_opportunity & lru_caches[10][2].perfect_hit_opportunity
	
		lfu_perfect_opportunity_20 = lfu_caches[9][0].perfect_hit_opportunity & lfu_caches[10][0].perfect_hit_opportunity
		lfu_perfect_opportunity_5  = lfu_caches[9][1].perfect_hit_opportunity & lfu_caches[10][1].perfect_hit_opportunity
		lfu_perfect_opportunity_1  = lfu_caches[9][2].perfect_hit_opportunity & lfu_caches[10][2].perfect_hit_opportunity

		lru_perfect_opportunities_20 += lru_perfect_opportunity_20
		lru_perfect_opportunities_5  += lru_perfect_opportunity_5
		lru_perfect_opportunities_1  += lru_perfect_opportunity_1

		lfu_perfect_opportunities_20 += lfu_perfect_opportunity_20
		lfu_perfect_opportunities_5  += lfu_perfect_opportunity_5
		lfu_perfect_opportunities_1  += lfu_perfect_opportunity_1

		unique_values[0].add(values[0])

		if line_num % 100000 == 0:
			print(line_num)
		line_num += 1

	f.close()

	for idx in range(NUM_EMBEDDING_TABLES):
		print("embedding_table: ", idx)

		print("LRU cache: ")
		for lru_idx in range(3):
			lru_cache = lru_caches[idx][lru_idx]
			print("cache_size: ", lru_cache.capacity)
			print("lru_cache_{hit, miss}: ", lru_cache.cache_hit, lru_cache.cache_miss)
			print("lru_cache_hit_rate: ", 1.0 * (lru_cache.cache_hit) / (lru_cache.cache_hit + lru_cache.cache_miss + 1))
			print("\n")

		print("LFU cache: ")
		for lfu_idx in range(3):
			lfu_cache = lfu_caches[idx][lfu_idx]
			print("cache_size: ", lfu_cache.capacity)
			print("lru_cache_{hit, miss}: ", lfu_cache.cache_hit, lfu_cache.cache_miss)
			print("lru_cache_hit_rate: ", 1.0 * (lfu_cache.cache_hit) / (lfu_cache.cache_hit + lfu_cache.cache_miss + 1))
			print("\n")
		print("\n\n\n")

	for key in unique_values:
		print(f"Embedding table {key} has {len(unique_values[key])} unique values")

	print("Perfect hits: ")
	print(lru_perfect_hits_20, lru_perfect_opportunities_20, 1.0 * lru_perfect_hits_20 / lru_perfect_opportunities_20)
	print(lru_perfect_hits_5, lru_perfect_opportunities_5, 1.0 * lru_perfect_hits_5 / lru_perfect_opportunities_5)
	print(lru_perfect_hits_1, lru_perfect_opportunities_1, 1.0 * lru_perfect_hits_1 / lru_perfect_opportunities_1)

	print(lfu_perfect_hits_20, lfu_perfect_opportunities_20, 1.0 * lfu_perfect_hits_20 / lfu_perfect_opportunities_20)
	print(lfu_perfect_hits_5, lfu_perfect_opportunities_5, 1.0 * lfu_perfect_hits_5 / lfu_perfect_opportunities_5)
	print(lfu_perfect_hits_1, lfu_perfect_opportunities_1, 1.0 * lfu_perfect_hits_1 / lfu_perfect_opportunities_1)

if __name__ == "__main__":
	main("train")