Place data files into this folder for reproducing results.

Data findings

Number of categorical variables: 22

Note: the first variable (advertisement identifier) is technically a categorical variable,
but it contains as many unique values as number of rows, so it always results in a cache miss.
We (probably) don't want to include it in the results.

Number of rows:
- Train: 40428968
- Test: 4577465

Organization
- `pre_results` => data before validation
- `result` => final results to be reported