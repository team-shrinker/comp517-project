# Avazu Trace Testing

Two files are used for trace testing the Avazu dataset: `trace_testing_avazu_fc_perfect_hit.py` and `trace_testing_perfect_hit.py`.

Both files do a full run through of the specified dataset and return both both normal hit rates and perfect hits rates. `trace_testing_avazu_fc_perfect_hit.py` specifically applies the flat cache design advocated by the Fleche system (Eurosys 24) while `trace_testing_perfect_hit.py` applies our technique.

## Reproducting results steps

1. First, you need to download and extract the Avazu dataset from this [link](https://www.kaggle.com/competitions/avazu-ctr-prediction/data). The extracted files should be named `train.csv` and `test.csv`.
2. Then, you can run either of the scripts specified above: `python3 trace_testing_perfect_hit.py`. By default, the script uses the test dataset. Replace the `main("test")` with `main("train")` to apply the training dataset. Each script will return both the LRU/LFU results so you don't need to specify the cache replacement algorithm.

Since Avazu is a fairly small dataset (compared to Criteo Kaggle/TB), we've provided the full set of results for the train/test datasets and for both the LRU/LFU algorithms.