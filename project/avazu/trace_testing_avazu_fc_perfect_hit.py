from collections import OrderedDict, defaultdict

# one embedding table per categorical variable
NUM_EMBEDDING_TABLES = 22

total_unique_vals_test = 1386598
total_unique_vals_train = 9449226

class LRUCache(OrderedDict):
	def __init__(self, capacity):
		"""
		:type capacity: int
		"""
		self.capacity   = capacity
		self.cache_hit  = 0
		self.cache_miss = 0
		self.last_hit = -1
		self.perfect_hit_opportunity = 0
	def get(self, key):
		"""
		:type key: int
		:rtype: int
		"""
		if key not in self:
			return -1
		self.move_to_end(key)
		return self[key]
	def put(self, key, value):
		"""
		:type key: int
		:type value: int
		:rtype: void
		"""
		if key in self:
			# only count hits when the cache is filled
			# (not during process of filling up)
			if len(self) == self.capacity: 
				self.cache_hit += 1
				self.last_hit = 1
				self.perfect_hit_opportunity = 1
			self.move_to_end(key)
		else:
			# only count misses when the cache is filled
			# (not during process of filling up)
			if len(self) == self.capacity:
				self.cache_miss += 1
				self.last_hit = 0
				self.perfect_hit_opportunity = 1
		self[key] = value
		if len(self) > self.capacity:
			self.popitem(last = False)

class LFUCache:
	def __init__(self, capacity: int):
		self.capacity=capacity
		self.minfreq=None
		self.keyfreq={}
		self.freqkeys=defaultdict(OrderedDict)
		self.cache_hit = 0
		self.cache_miss = 0
		self.last_hit = -1
		self.perfect_hit_opportunity = 0
	def get(self, key: int) -> int:
		if key not in self.keyfreq:
			return -1
		freq=self.keyfreq[key]
		val=self.freqkeys[freq][key]
		del self.freqkeys[freq][key]
		if not self.freqkeys[freq]:
			if freq==self.minfreq:
				self.minfreq+=1
			del self.freqkeys[freq]
		self.keyfreq[key]=freq+1
		self.freqkeys[freq+1][key]=val
		return val
	def put(self, key: int, value: int) -> None:
		if self.capacity<=0:
			return
		if key in self.keyfreq:
			# only count hits when the cache is filled
			# (not during process of filling up)
			if self.capacity==len(self.keyfreq):
				self.cache_hit += 1
				self.last_hit = 1
				self.perfect_hit_opportunity = 1
			freq=self.keyfreq[key]
			self.freqkeys[freq][key]=value
			self.get(key)
			return
		if self.capacity==len(self.keyfreq):
			# only count misses when the cache is filled
			# (not during process of filling up)
			self.cache_miss += 1
			self.last_hit = 0
			self.perfect_hit_opportunity = 1
			delkey,delval=self.freqkeys[self.minfreq].popitem(last=False)
			del self.keyfreq[delkey]
		self.keyfreq[key]=1
		self.freqkeys[1][key]=value
		self.minfreq=1

def main(data_source = "train"):
	if data_source not in { "train", "test" }:
		raise Exception("data_source must be in { \"train\", \"test\" }")

	if data_source == "train":
		lru_caches = [
			LRUCache(int(0.2 * total_unique_vals_train)),
			LRUCache(int(0.05 * total_unique_vals_train)),
			LRUCache(int(0.01 * total_unique_vals_train))
		]
		lfu_caches = [
			LFUCache(int(0.2 * total_unique_vals_train) + 1),
			LFUCache(int(0.05 * total_unique_vals_train) + 1),
			LFUCache(int(0.01 * total_unique_vals_train) + 1)
		]
		f = open('data/train.csv', 'r')
	else:
		lru_caches = [
			LRUCache(int(0.2 * total_unique_vals_test)),
			LRUCache(int(0.05 * total_unique_vals_test)),
			LRUCache(int(0.01 * total_unique_vals_test))
		]
		lfu_caches = [
			LFUCache(int(0.2 * total_unique_vals_test) + 1),
			LFUCache(int(0.05 * total_unique_vals_test) + 1),
			LFUCache(int(0.01 * total_unique_vals_test) + 1)
		]
		f = open('data/test.csv', 'r')
	
	unique_values = defaultdict(set)

	lru_perfect_hits = [0,0,0]
	lfu_perfect_hits = [0,0,0]

	lru_perfect_opportunities = [0,0,0]
	lfu_perfect_opportunities = [0,0,0]

	line_num = 0
	for line in f:
		lru_perfect_hit = [1,1,1]
		lfu_perfect_hit = [1,1,1]

		lru_perfect_opportunity = [1,1,1]
		lfu_perfect_opportunity = [1,1,1]

		# Split the line into a list of values using the separator
		pre_values = line.split(',') # TODO: remove '\n' from the last word
		if data_source == "train":
			values = [pre_values[0]] + pre_values[3:]
		else:
			values = [pre_values[0]] + pre_values[2:]
		for idx in range(NUM_EMBEDDING_TABLES):
			value = values[idx]
			value = str(idx) + "-" + value
			unique_values[idx].add(value)
			for cache_idx in range(3):
				lru_caches[cache_idx].put(value, 1)
				lfu_caches[cache_idx].put(value, 1)

				lru_perfect_hit[cache_idx] = lru_perfect_hit[cache_idx] & lru_caches[cache_idx].last_hit
				lfu_perfect_hit[cache_idx] = lfu_perfect_hit[cache_idx] & lfu_caches[cache_idx].last_hit

				lru_perfect_opportunity[cache_idx] = lru_perfect_opportunity[cache_idx] & lru_caches[cache_idx].perfect_hit_opportunity
				lfu_perfect_opportunity[cache_idx] = lfu_perfect_opportunity[cache_idx] & lfu_caches[cache_idx].perfect_hit_opportunity

		for cache_idx in range(3):
			lru_perfect_hits[cache_idx]          = lru_perfect_hits[cache_idx] + lru_perfect_hit[cache_idx]
			lru_perfect_opportunities[cache_idx] = lru_perfect_opportunities[cache_idx] + lru_perfect_opportunity[cache_idx]

			lfu_perfect_hits[cache_idx]          = lfu_perfect_hits[cache_idx] + lfu_perfect_hit[cache_idx]
			lfu_perfect_opportunities[cache_idx] = lfu_perfect_opportunities[cache_idx] + lfu_perfect_opportunity[cache_idx]

		unique_values[0].add(values[0])

		if line_num % 100000 == 0:
			print(line_num)
		line_num += 1

	f.close()

	for lru_idx in range(3):
		lru_cache = lru_caches[lru_idx]
		print("cache_size: ", lru_cache.capacity)
		print("lru_cache_{hit, miss}: ", lru_cache.cache_hit, lru_cache.cache_miss)
		print("lru_cache_hit_rate: ", 1.0 * (lru_cache.cache_hit) / (lru_cache.cache_hit + lru_cache.cache_miss + 1))
		print("\n")

	print("LFU cache: ")
	for lfu_idx in range(3):
		lfu_cache = lfu_caches[lfu_idx]
		print("cache_size: ", lfu_cache.capacity)
		print("lru_cache_{hit, miss}: ", lfu_cache.cache_hit, lfu_cache.cache_miss)
		print("lru_cache_hit_rate: ", 1.0 * (lfu_cache.cache_hit) / (lfu_cache.cache_hit + lfu_cache.cache_miss + 1))
		print("\n")
	print("\n\n\n")

	print("Printing perfect hit rates...")
	for cache_idx in range(3):
		print(lru_perfect_hits[cache_idx], lru_perfect_opportunities[cache_idx], 1.0 * lru_perfect_hits[cache_idx] / lru_perfect_opportunities[cache_idx])
		print(lfu_perfect_hits[cache_idx], lfu_perfect_opportunities[cache_idx], 1.0 * lfu_perfect_hits[cache_idx] / lfu_perfect_opportunities[cache_idx])

if __name__ == "__main__":
	main("train")
