percent perfect hits 20:  0.6572397008673259
percent perfect hits 5:  0.5896377687688209
percent perfect hits 1:  0.48153558303480476

embedding_table:  0
cache_size:  270
lru_cache_{hit, miss}:  6009603 32262
lru_cache_hit_rate:  0.9946602580494599
cache_size:  67
lru_cache_{hit, miss}:  5904699 137369
lru_cache_hit_rate:  0.9772645723285471
cache_size:  13
lru_cache_{hit, miss}:  5364428 677694
lru_cache_hit_rate:  0.8878384117368038




embedding_table:  1
cache_size:  110
lru_cache_{hit, miss}:  4925921 1116104
lru_cache_hit_rate:  0.815276500842019
cache_size:  27
lru_cache_{hit, miss}:  2676931 3365177
lru_cache_hit_rate:  0.44304587074577284
cache_size:  5
lru_cache_{hit, miss}:  765437 5276693
lru_cache_hit_rate:  0.12668330539064865




embedding_table:  2
cache_size:  330920
lru_cache_{hit, miss}:  4009766 1498157
lru_cache_hit_rate:  0.7279996470538894
cache_size:  82730
lru_cache_{hit, miss}:  3680770 2075343
lru_cache_hit_rate:  0.6394540899388181
cache_size:  16546
lru_cache_{hit, miss}:  3156131 2666166
lru_cache_hit_rate:  0.5420766065351871




embedding_table:  3
cache_size:  99390
lru_cache_{hit, miss}:  5160130 579323
lru_cache_hit_rate:  0.8990630291771707
cache_size:  24847
lru_cache_{hit, miss}:  4734299 1079697
lru_cache_hit_rate:  0.814293473886119
cache_size:  4969
lru_cache_{hit, miss}:  3951118 1882756
lru_cache_hit_rate:  0.6772717408706461




embedding_table:  4
cache_size:  56
lru_cache_{hit, miss}:  6027448 14631
lru_cache_hit_rate:  0.9975784825057733
cache_size:  14
lru_cache_{hit, miss}:  5912053 130068
lru_cache_hit_rate:  0.9784731222694812
cache_size:  2
lru_cache_{hit, miss}:  4307517 1734616
lru_cache_hit_rate:  0.712913303960704




embedding_table:  5
cache_size:  4
lru_cache_{hit, miss}:  5138844 415865
lru_cache_hit_rate:  0.9251328917500449
cache_size:  1
lru_cache_{hit, miss}:  1693851 3860861
lru_cache_hit_rate:  0.30493948201094856
cache_size:  0
lru_cache_{hit, miss}:  0 5554713
lru_cache_hit_rate:  0.0




embedding_table:  6
cache_size:  2363
lru_cache_{hit, miss}:  4538566 1501206
lru_cache_hit_rate:  0.7514465777847243
cache_size:  590
lru_cache_{hit, miss}:  2493934 3547611
lru_cache_hit_rate:  0.4127973887474148
cache_size:  118
lru_cache_{hit, miss}:  975667 5066350
lru_cache_hit_rate:  0.16148034671203343




embedding_table:  7
cache_size:  119
lru_cache_{hit, miss}:  6023380 18636
lru_cache_hit_rate:  0.9969155990318463
cache_size:  29
lru_cache_{hit, miss}:  5925703 116403
lru_cache_hit_rate:  0.9807346974713783
cache_size:  5
lru_cache_{hit, miss}:  5090712 951418
lru_cache_hit_rate:  0.8425359931017704




embedding_table:  8
cache_size:  0
lru_cache_{hit, miss}:  0 6042135
lru_cache_hit_rate:  0.0
cache_size:  0
lru_cache_{hit, miss}:  0 6042135
lru_cache_hit_rate:  0.0
cache_size:  0
lru_cache_{hit, miss}:  0 6042135
lru_cache_hit_rate:  0.0




embedding_table:  9
cache_size:  11115
lru_cache_{hit, miss}:  5655984 375036
lru_cache_hit_rate:  0.9378154938965548
cache_size:  2778
lru_cache_{hit, miss}:  4592369 1446988
lru_cache_hit_rate:  0.7604069439842686
cache_size:  555
lru_cache_{hit, miss}:  2987343 3054237
lru_cache_hit_rate:  0.49446386541269005




embedding_table:  10
cache_size:  970
lru_cache_{hit, miss}:  4476116 1565049
lru_cache_hit_rate:  0.7409358956426451
cache_size:  242
lru_cache_{hit, miss}:  2401930 3639963
lru_cache_hit_rate:  0.3975459346929845
cache_size:  48
lru_cache_{hit, miss}:  877022 5165065
lru_cache_hit_rate:  0.14515216348258475




embedding_table:  11
cache_size:  285393
lru_cache_{hit, miss}:  4232447 1321003
lru_cache_hit_rate:  0.7621293070073558
cache_size:  71348
lru_cache_{hit, miss}:  3893525 1873970
lru_cache_hit_rate:  0.6750807759694634
cache_size:  14269
lru_cache_{hit, miss}:  3350916 2473658
lru_cache_hit_rate:  0.5753066232826641




embedding_table:  12
cache_size:  625
lru_cache_{hit, miss}:  4169587 1871923
lru_cache_hit_rate:  0.6901564344013335
cache_size:  156
lru_cache_{hit, miss}:  2149902 3892077
lru_cache_hit_rate:  0.3558274532235216
cache_size:  31
lru_cache_{hit, miss}:  726835 5315269
lru_cache_hit_rate:  0.12029501643798253




embedding_table:  13
cache_size:  5
lru_cache_{hit, miss}:  5293523 748607
lru_cache_hit_rate:  0.8761021361672126
cache_size:  1
lru_cache_{hit, miss}:  1609021 4433113
lru_cache_hit_rate:  0.26630011846807766
cache_size:  0
lru_cache_{hit, miss}:  0 6042135
lru_cache_hit_rate:  0.0




embedding_table:  14
cache_size:  2429
lru_cache_{hit, miss}:  5593382 446324
lru_cache_hit_rate:  0.9261017009768356
cache_size:  607
lru_cache_{hit, miss}:  3967236 2074292
lru_cache_hit_rate:  0.6566610301235052
cache_size:  121
lru_cache_{hit, miss}:  1594870 4447144
lru_cache_hit_rate:  0.26396330759908865




embedding_table:  15
cache_size:  205933
lru_cache_{hit, miss}:  4620257 1012653
lru_cache_hit_rate:  0.8202256027523962
cache_size:  51483
lru_cache_{hit, miss}:  4252075 1535285
lru_cache_hit_rate:  0.7347175568825854
cache_size:  10296
lru_cache_{hit, miss}:  3647993 2180554
lru_cache_hit_rate:  0.625883775150136




embedding_table:  16
cache_size:  2
lru_cache_{hit, miss}:  2755802 3286331
lru_cache_hit_rate:  0.4560975403884688
cache_size:  0
lru_cache_{hit, miss}:  0 6042135
lru_cache_hit_rate:  0.0
cache_size:  0
lru_cache_{hit, miss}:  0 6042135
lru_cache_hit_rate:  0.0




embedding_table:  17
cache_size:  973
lru_cache_{hit, miss}:  5641787 399375
lru_cache_hit_rate:  0.9338910295734496
cache_size:  243
lru_cache_{hit, miss}:  4064778 1977114
lru_cache_hit_rate:  0.6727657495367345
cache_size:  48
lru_cache_{hit, miss}:  1703768 4338319
lru_cache_hit_rate:  0.2819833610472673




embedding_table:  18
cache_size:  415
lru_cache_{hit, miss}:  3170024 207472
lru_cache_hit_rate:  0.9385722440529908
cache_size:  103
lru_cache_{hit, miss}:  2784208 593600
lru_cache_hit_rate:  0.8242647302629398
cache_size:  20
lru_cache_{hit, miss}:  2348672 1029219
lru_cache_hit_rate:  0.6953072198007574




embedding_table:  19
cache_size:  0
lru_cache_{hit, miss}:  0 3377911
lru_cache_hit_rate:  0.0
cache_size:  0
lru_cache_{hit, miss}:  0 3377911
lru_cache_hit_rate:  0.0
cache_size:  0
lru_cache_{hit, miss}:  0 3377911
lru_cache_hit_rate:  0.0




embedding_table:  20
cache_size:  250963
lru_cache_{hit, miss}:  4399887 1187993
lru_cache_hit_rate:  0.7873982619526547
cache_size:  62740
lru_cache_{hit, miss}:  4051270 1724833
lru_cache_hit_rate:  0.7013846532861343
cache_size:  12548
lru_cache_{hit, miss}:  3489677 2336618
lru_cache_hit_rate:  0.5989530224611009




embedding_table:  21
cache_size:  3
lru_cache_{hit, miss}:  1383804 86893
lru_cache_hit_rate:  0.9409171297690823
cache_size:  0
lru_cache_{hit, miss}:  0 1470700
lru_cache_hit_rate:  0.0
cache_size:  0
lru_cache_{hit, miss}:  0 1470700
lru_cache_hit_rate:  0.0




embedding_table:  22
cache_size:  2
lru_cache_{hit, miss}:  2845650 3196483
lru_cache_hit_rate:  0.47096778571408476
cache_size:  0
lru_cache_{hit, miss}:  0 6042135
lru_cache_hit_rate:  0.0
cache_size:  0
lru_cache_{hit, miss}:  0 6042135
lru_cache_hit_rate:  0.0




embedding_table:  23
cache_size:  21549
lru_cache_{hit, miss}:  5574177 243117
lru_cache_hit_rate:  0.9582078884099721
cache_size:  5387
lru_cache_{hit, miss}:  5125009 708447
lru_cache_hit_rate:  0.878554496682584
cache_size:  1077
lru_cache_{hit, miss}:  4210991 1626775
lru_cache_hit_rate:  0.7213360384777328




embedding_table:  24
cache_size:  18
lru_cache_{hit, miss}:  3278541 99352
lru_cache_hit_rate:  0.9705875822591183
cache_size:  4
lru_cache_{hit, miss}:  1665693 1712214
lru_cache_hit_rate:  0.49311393120059255
cache_size:  0
lru_cache_{hit, miss}:  0 3377911
lru_cache_hit_rate:  0.0




embedding_table:  25
cache_size:  13827
lru_cache_{hit, miss}:  5819489 208819
lru_cache_hit_rate:  0.9653602636096231
cache_size:  3456
lru_cache_{hit, miss}:  5427460 611219
lru_cache_hit_rate:  0.8987826642217611
cache_size:  691
lru_cache_{hit, miss}:  4721564 1319880
lru_cache_hit_rate:  0.7815290516638075
