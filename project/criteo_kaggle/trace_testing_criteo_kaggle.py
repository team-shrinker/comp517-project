from collections import OrderedDict, defaultdict
class LRUCache(OrderedDict):
    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.capacity   = capacity
        self.cache_hit  = 0
        self.cache_miss = 0
        self.last_hit = 0
    def get(self, key):
        """
        :type key: int
        :rtype: int
        """
        if key not in self:
            return -1
        self.move_to_end(key)
        return self[key]
    def put(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: void
        """
        if key in self:
            self.last_hit = 1
            self.cache_hit += 1
            self.move_to_end(key)
        else: # already moved to end
            self.last_hit = 0
            # dont count initially putting elements into cache as misses
            if len(self) == self.capacity:
                self.cache_miss += 1
        self[key] = value
        if len(self) > self.capacity:
            self.popitem(last = False)

class LFUCache:
    def __init__(self, capacity: int):
        self.capacity=capacity
        self.minfreq=None
        self.keyfreq={}
        self.freqkeys=defaultdict(OrderedDict)
        self.cache_hit = 0
        self.cache_miss = 0
        self.last_hit = 0

    def get(self, key: int) -> int:
        if key not in self.keyfreq:
            return -1
        freq=self.keyfreq[key]
        val=self.freqkeys[freq][key]
        del self.freqkeys[freq][key]
        if not self.freqkeys[freq]:
            if freq==self.minfreq:
                self.minfreq+=1
            del self.freqkeys[freq]
        self.keyfreq[key]=freq+1
        self.freqkeys[freq+1][key]=val
        return val

    def put(self, key: int, value: int) -> None:
        if self.capacity<=0:
            return
        if key in self.keyfreq:
            if self.capacity==len(self.keyfreq):
                self.cache_hit += 1
            self.last_hit = 1
            freq=self.keyfreq[key]
            self.freqkeys[freq][key]=value
            self.get(key)
            return
        else:
            self.last_hit = 0
        if self.capacity==len(self.keyfreq):
            self.cache_miss += 1
            delkey,delval=self.freqkeys[self.minfreq].popitem(last=False)
            del self.keyfreq[delkey]
        self.keyfreq[key]=1
        self.freqkeys[1][key]=value
        self.minfreq=1

num_unique_vals_train = {
    0: 1460,
    1: 583,
    2: 10131227,
    3: 2202608,
    4: 305,
    5: 24,
    6: 12517,
    7: 633,
    8: 3,
    9: 93145,
    10: 5683,
    11: 8351593,
    12: 3194,
    13: 27,
    14: 14992,
    15: 5461306,
    16: 10,
    17: 5652,
    18: 2173,
    19: 4,
    20: 7046547,
    21: 18,
    22: 15, 
    23: 286181,
    24: 105,
    25: 142572
}    

num_unique_vals_test = {
    0: 1351,
    1: 554,
    2: 1654604,
    3: 496952,
    4: 280,
    5: 23,
    6: 11815,
    7: 596,
    8: 3,
    9: 55575,
    10: 4852,
    11: 1426969,
    12: 3129,
    13: 27,
    14: 12147,
    15: 1029668,
    16: 10,
    17: 4866,
    18: 2077,
    19: 4,
    20: 1254819,
    21: 18,
    22: 14, 
    23: 107746,
    24: 90,
    25: 69136
}    

def main(cache_type="lru", dataset_type="test"):
    if dataset_type == "train":
        num_unique_vals = num_unique_vals_train
        file_offset = 14
        f = open("data/train.txt")
    elif dataset_type == "test":
        num_unique_vals = num_unique_vals_test
        file_offset = 13
        f = open("data/test.txt")
    else:
        raise Exception("dataset_type must be train or test")

    if cache_type == "lru": 
        caches = [
            [LRUCache(int(0.2 * num_unique_vals[idx])), LRUCache(int(0.05 * num_unique_vals[idx])), LRUCache(int(0.01 * num_unique_vals[idx]))] 
                for idx in range(26)
        ]
    elif cache_type == "lfu":
        caches = [
            [LFUCache(int(0.2 * num_unique_vals_train[idx]) + 1), LFUCache(int(0.05 * num_unique_vals_train[idx]) + 1), LFUCache(int(0.01 * num_unique_vals_train[idx]) + 1)] 
                for idx in range(26)
        ]
    else:
        raise Exception("cache_type must be either lru or lfu")

    line_num = 0
    perfect_hits_20, perfect_hits_5, perfect_hits_1 = 0, 0, 0
    for line in f:
        # Split the line into a list of values using the separator
        values = line.split('\t') # TODO: remove '\n' from the last word
        for table_idx in range(26):
            value = values[table_idx + file_offset]
            if value == '':
                caches[table_idx][0].last_hit = 0
                caches[table_idx][1].last_hit = 0
                caches[table_idx][2].last_hit = 0
                continue

            for cache_idx in range(3):
                cache = caches[table_idx][cache_idx]
                cache.put(value, 1)

        perfect_hit_20 = caches[2][0].last_hit & caches[11][0].last_hit & caches[20][0].last_hit & caches[15][0].last_hit & caches[3][0].last_hit
        perfect_hit_5 = caches[2][1].last_hit & caches[11][1].last_hit & caches[20][1].last_hit & caches[15][1].last_hit & caches[3][1].last_hit
        perfect_hit_1 = caches[2][2].last_hit & caches[11][2].last_hit & caches[20][2].last_hit & caches[15][2].last_hit & caches[3][2].last_hit

        if line_num % 1000000 == 0:
            print(line_num)
        line_num += 1

        perfect_hits_20 += perfect_hit_20
        perfect_hits_5 += perfect_hit_5
        perfect_hits_1 += perfect_hit_1

    f.close()

    print("percent perfect hits 20: ", 1.0 * perfect_hits_20 / line_num)
    print("percent perfect hits 5: ", 1.0 * perfect_hits_5 / line_num)
    print("percent perfect hits 1: ", 1.0 * perfect_hits_1 / line_num)

    for idx in range(26):
        print("embedding_table: ", idx)
        for cache_idx in range(3):
            cache = caches[idx][cache_idx]
            print("cache_size: ", cache.capacity)
            print("lru_cache_{hit, miss}: ", cache.cache_hit, cache.cache_miss)
            print("lru_cache_hit_rate: ", 1.0 * (cache.cache_hit) / (cache.cache_hit + cache.cache_miss))
        print("\n\n\n")

if __name__ == "__main__":
    main()
