# Trace Testing Criteo Kaggle

The file `trace_testing_criteo_kaggle.py` does calculation to determine both the hit rate and perfect hit rate for the Criteo Kaggle dataset. This single script can be modified to try either the LRU/LFU cache replacement algorithm and to use either the train or test (which is smaller and better for quick verification) datasets.

## Reproducing results steps

1. First, you need to download the Criteo Kaggle dataset from this [link](https://www.kaggle.com/datasets/mrkmakr/criteo-dataset). Download and unzip the data files into the `data/` subdirectory. The train and test files should be named `train.txt` and `test.txt`, respectively.
2. You then need to run the script: `python3 trace_testing_criteo_kaggle.py`. By default, this script runs the tracing for the test dataset using the LRU cache replacement policy algorithm. In the `main()` function in the script, you can change the `cache_type` and `dataset_type` parameters to trace other configuration.

In the `results` subdirectory you'll find the results of the tracing on the test dataset using the LRU algorithm.