# Trace Testing Criteo TB

The tracing files here correspond to the Criteo TB dataset. This is the largest dataset.

For this set of experiments, we aimed to determine the perfect hit rate afforded by our caching design and compare that against EVStore.

## Reproducing results steps

1. First, you need to obtain the data. Unlike the other trace tests, this one requires a specially formatted dataset that contains 3 full days of Criteo TB data that can be ingested by our ported version of EVStore (~90GB of data). Please talk to us to provide you that data. The file name is `output_tb_non_null_lines_0.txt`. Place that file under the data `subdirectory`.
2. `trace_testing_criteo_tb.py` contains the trace of our caching strategy while `trace_testing_criteo_tb_evlfu.py` contains the trace of EVStore's LFU implementation. Running is simple:
    - `python3 trace_testing_criteo_tb.py`
    - `python3 trace_testing_criteo_tb_evlfu.py`
The EVStore implementation only has the LFU cache replacement algorithm, so we also only report results of our technique with LFU for a fair comparison. In the `main()` function call, you can add the `cache_type=lru` parameter to try out the LRU implementation.
3. The file `EvLFU_C1.py` comes from EVStore's Github repository. We do not take claim for this code, but we did add minor modifications in order to count the number of perfect hits. 
