from collections import OrderedDict, defaultdict
class LRUCache(OrderedDict):
    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.capacity   = capacity
        self.cache_hit  = 0
        self.cache_miss = 0
    def get(self, key):
        """"
        :type key: int
        :rtype: int
        """
        if key not in self:
            return -1
        self.move_to_end(key)
        return self[key]
    def put(self, key, value):
        """"
        :type key: int
        :type value: int
        :rtype: void
        """
        if key in self:
            self.cache_hit += 1
            self.move_to_end(key)
        else: # already moved to end
            # dont count initially putting elements into cache as misses
            if len(self) == self.capacity:
                self.cache_miss += 1
        self[key] = value
        if len(self) > self.capacity:
            self.popitem(last = False)
class LFUCache:
    def __init__(self, capacity: int):
        self.capacity=capacity
        self.minfreq=None
        self.keyfreq={}
        self.freqkeys=defaultdict(OrderedDict)
        self.last_hit=None
    def get(self, key: int) -> int:
        if key not in self.keyfreq:
            return -1
        freq=self.keyfreq[key]
        val=self.freqkeys[freq][key]
        del self.freqkeys[freq][key]
        if not self.freqkeys[freq]:
            if freq==self.minfreq:
                self.minfreq+=1
            del self.freqkeys[freq]
        self.keyfreq[key]=freq+1
        self.freqkeys[freq+1][key]=val
        return val
    def put(self, key: int, value: int) -> None:
        if self.capacity<=0:
            return

        if key in self.keyfreq:
            self.last_hit = 1
            freq=self.keyfreq[key]
            self.freqkeys[freq][key]=value
            self.get(key)
            return
        else:
            self.last_hit = 0

        if self.capacity==len(self.keyfreq):
            delkey,delval=self.freqkeys[self.minfreq].popitem(last=False)
            del self.keyfreq[delkey]
        self.keyfreq[key]=1
        self.freqkeys[1][key]=value
        self.minfreq=1

num_unique_vals = {
    0: 45761103, # 46gb
    1: 31833,
    2: 15421,
    3: 7295,
    4: 19901,
    5: 3,
    6: 6566,
    7: 1363,
    8: 62,
    9: 27777159, # 28gb
    10: 1177639,
    11: 274314,
    12: 10,
    13: 2209,
    14: 10268,
    15: 89,
    16: 4,
    17: 963,
    18: 14,
    19: 55838074, # 56gb
    20: 9738987, # 10gb
    21: 38711885, # 39gb
    22: 381897,
    23: 11312,
    24: 102,
    25: 35
}

def main(cache_type="lfu"):
    largest_table_indexes = [0, 9, 19, 20, 21]
    if cache_type == "lfu":
        caches = [
            [
                LFUCache(int(0.2 * num_unique_vals[val]) + 1),
                LFUCache(int(0.05 * num_unique_vals[val]) + 1),
                LFUCache(int(0.01 * num_unique_vals[val]) + 1)
            ] for val in largest_table_indexes
        ]
    elif cache_type == "lru":
        caches = [
            [
                LFUCache(int(0.2 * num_unique_vals[val]) + 1),
                LFUCache(int(0.05 * num_unique_vals[val]) + 1),
                LFUCache(int(0.01 * num_unique_vals[val]) + 1)
            ] for val in largest_table_indexes
        ]
    else:
        raise Exception("cache_type must be either lfu or lru")
    f = open('data/output_tb_non_null_lines_0.txt', 'r')

    line_num = 0
    perfect_hits_20, perfect_hits_5, perfect_hits_1 = 0, 0, 0
    for line in f:
        # Split the line into a list of values using the separator
        values = line.split('\t')

        for idx in range(len(largest_table_indexes)):
            specific_table_caches = caches[idx]
            table_idx = largest_table_indexes[idx]
            value_to_put = values[table_idx + 14]

            for cache in specific_table_caches:
                cache.put(value_to_put, 1)

        perfect_hit_20 = caches[0][0].last_hit & caches[1][0].last_hit & caches[2][0].last_hit & caches[3][0].last_hit & caches[4][0].last_hit
        perfect_hit_5 = caches[0][1].last_hit & caches[1][1].last_hit & caches[2][1].last_hit & caches[3][1].last_hit & caches[4][1].last_hit
        perfect_hit_1 = caches[0][2].last_hit & caches[1][2].last_hit & caches[2][2].last_hit & caches[3][2].last_hit & caches[4][2].last_hit

        perfect_hits_20 += perfect_hit_20
        perfect_hits_5 += perfect_hit_5
        perfect_hits_1 += perfect_hit_1

        if line_num % 1000000 == 0:
            print("Iteration: ", line_num)
            print("percent perfect hits 20: ", 1.0 * perfect_hit_20 / line_num)
            print(perfect_hits_5)
            print(perfect_hits_1)
        line_num += 1
        
    f.close()

    print("percent perfect hits 20: ", 1.0 * perfect_hits_20 / line_num)
    print("percent perfect hits 5: ", 1.0 * perfect_hits_5 / line_num)
    print("percent perfect hits 1: ", 1.0 * perfect_hits_1 / line_num)

if __name__ == "__main__":
    main()
