from EvLFU_C1 import EvLFU

num_unique_vectors = 179772504

lfu_caches = [
    EvLFU(int(0.2 * num_unique_vectors) + 1),
    EvLFU(int(0.05 * num_unique_vectors) + 1),
    EvLFU(int(0.01 * num_unique_vectors) + 1)
]

f = open('data/output_tb_non_null_lines_0.txt', 'r')
line_num = 1
perfect_hits_20, perfect_hits_5, perfect_hits_1 = 0, 0, 0
for line in f:
    # Split the line into a list of values using the separator
    values = line.split('\t')
      
    lfu_caches[0].request_to_ev_lfu(values[14:])
    lfu_caches[1].request_to_ev_lfu(values[14:])
    lfu_caches[2].request_to_ev_lfu(values[14:])

    if line_num % 100000 == 0:
        print(line_num)
        print(1.0 * lfu_caches[0].num_perfect_hits / line_num)
        print(1.0 * lfu_caches[1].num_perfect_hits / line_num)
        print(1.0 * lfu_caches[2].num_perfect_hits / line_num)
        print("\n")
    line_num += 1
    
f.close()

print("line_num at end: ", line_num)
total_lines = 352369841
print("percent perfect hits 20: ", 1.0 * lfu_caches[0].num_perfect_hits / line_num)
print("percent perfect hits 5: ", 1.0* lfu_caches[1].num_perfect_hits / line_num)
print("percent perfect hits 1: ", 1.0* lfu_caches[2].num_perfect_hits / line_num)