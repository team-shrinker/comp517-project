# COMP517 Project

Repository on our COMP517 on improving embedding tabling caching for deep learning recommendation models (DLRMs).

Please go to the `project` directory for instructions on how to reproduce results.

Work by:
- Wenyu (Eddy) Huang
- Hariharan Sezhiyan
- Shaochen (Henry) Zhong
